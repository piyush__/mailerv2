import React from 'react';

import Dashboard from './containers/Dashboard';

import './App.css';

const App = () => {
	return (
		<>
			<Dashboard />
		</>
	);
};

export default App;
