/* eslint-disable lines-between-class-members */
/** @jsx jsx */
import React, { Component } from 'react';
import { jsx, css } from '@emotion/core';
import api from '../../utils/api';
import isLocalHost from '../../utils/isLocalHost';
import classNames from 'classnames';

import Editor from '../Editor';
import IfElse from '../../components/IfElse';
import sortByDate from '../../utils/sortByDate';

import Mailer_Logo from '../../img/Mailer_Logo.png';
import Search_Logo from '../../img/search.png';

function getTemplateId(todo) {
	if (!todo.ref) {
		return null;
	}
	return todo.ref['@ref'].id;
}

const clipboard = text => {
	const newTempElem = document.createElement('input');

	newTempElem.style.opacity = 0;
	newTempElem.style.position = 'absolute';

	document.body.appendChild(newTempElem);
	newTempElem.value = text;
	newTempElem.select();
	document.execCommand('copy');
	document.body.removeChild(newTempElem);
};

export class Dashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			wordToMatch: '',
			templates: [],
			types: {
				all: true,
				buyhatke: false,
				informvisitors: false,
				salesla: false,
				tfs: false,
				bitbns: false,
				bidforx: false,
				umc: false,
				cricketSwag: false,
				others: false,
			},
			editor: {
				open: false,
				type: null,
				template: null,
			},
		};
	}

	componentDidMount() {
		// read all todos
		this.readAll();
	}

	componentDidUpdate(prevProps, prevState) {
		// console.log(prev.editor.open)
		if (prevState.editor.open) {
			// read all todos
			this.readAll();
		}
	}

	readAll = () => {
		api.readAll().then(templates => {
			if (templates.message === 'unauthorized') {
				if (isLocalHost()) {
					alert(
						'FaunaDB key is not unauthorized. Make sure you set it in terminal session where you ran `npm start`. Visit http://bit.ly/set-fauna-key for more info'
					);
				} else {
					alert(
						'FaunaDB key is not unauthorized. Verify the key `FAUNADB_SERVER_SECRET` set in Netlify enviroment variables is correct'
					);
				}
				return false;
			}
			this.setState({
				templates,
			});
		});
	};

	toggleEditor = () => {
		this.setState(({ editor }) => ({
			editor: Object.assign({}, editor, {
				open: !editor.open,
			}),
		}));
	};

	updatingRenderingArray = () => {
		const { templates, wordToMatch, types } = this.state;

		// checking whether there is any template or not
		if (!templates || !templates.length) {
			return [];
		}

		// sorting the templates by timestamp in descending order
		const timeStampKey = 'ts';
		const orderBy = 'desc';
		const sortOrder = sortByDate(timeStampKey, orderBy);
		const templatesByDate = templates.sort(sortOrder);

		// filter by type
		const filteredArray = types.all
			? templatesByDate
			: templatesByDate.filter(curr => types[curr.data.type]);

		// filter by wordToMatch
		const regex = new RegExp(wordToMatch, 'gi');
		return filteredArray.filter(
			curr => curr.data.name.match(regex) || curr.data.type.match(regex)
		);
	};

	CopyToClipboard = html => {
		clipboard(html);
		alert('HTML copied');
	};

	renderTemplates = () => {
		const filteredTemplates = this.updatingRenderingArray();

		return filteredTemplates.map((template, i) => {
			const { data, ref } = template;
			const { html } = data;
			const id = getTemplateId(template);

			let DeleteButton,
				EditButton,
				DuplicateButton,
				DownloadButton,
				CopyHtmlButton;

			if (ref) {
				DeleteButton = (
					<button
						type="button"
						data-id={id}
						onClick={this.deleteTemplate}
						className="c-btn c-btn--red mx-3"
					>
						Delete
					</button>
				);
				EditButton = (
					<button
						type="button"
						data-id={id}
						onClick={e => this.openEditor(e, 1)}
						className="c-btn c-btn--green mx-3"
					>
						Edit
					</button>
				);
				DuplicateButton = (
					<button
						type="button"
						data-id={id}
						onClick={e => this.openEditor(e, 2)}
						className="c-btn c-btn--green  mx-3"
					>
						Duplicate
					</button>
				);
				DownloadButton = (
					<button
						type="button"
						data-id={id}
						onClick={this.downloadTemplate}
						className="c-btn c-btn--red mx-3"
					>
						Download
					</button>
				);
				CopyHtmlButton = (
					<button
						type="button"
						data-id={id}
						onClick={() => this.CopyToClipboard(html)}
						className="c-btn c-btn--red mx-3"
					>
						Copy Html
					</button>
				);
			}

			return (
				<div key={i} className="thumbnail-wrapper">
					<div className="text-2xl capitalize">
						{data.name || 'undefined'}
					</div>
					<div className="capitalize text-gray-500">
						{data.type || 'undefined'}
					</div>
					<div className="flex flex-wrap py-2 justify-between -mx-3">
						{DownloadButton}
						{EditButton}
						{CopyHtmlButton}
						{DuplicateButton}
						{DeleteButton}
					</div>
				</div>
			);
		});
	};

	downloadTemplate = e => {
		const { templates } = this.state;
		const templateId = e.target.dataset.id;

		const filteredTemplate = templates.find(curr => {
			const currentId = getTemplateId(curr);
			return templateId === currentId;
		});

		const { name, html } = filteredTemplate.data;

		// download script
		fetch(`data:text/html;charset=utf-8,${encodeURIComponent(html)}`)
			.then(resp => resp.blob())
			.then(blob => {
				const url = window.URL.createObjectURL(blob);
				const a = document.createElement('a');
				a.style.display = 'none';
				a.href = url;
				// the filename you want
				a.download = name + '.html';
				document.body.appendChild(a);
				a.click();
				window.URL.revokeObjectURL(url);
				alert(`Downloaded ${name}.html`);
			})
			.catch(() => alert('oh no!'));
	};

	deleteTemplate = e => {
		if (window.confirm('Click Ok to delete template')) {
			const { templates } = this.state;
			const templateId = e.target.dataset.id;

			const filteredTemplates = templates.reduce(
				(acc, current) => {
					const currentId = getTemplateId(current);
					if (currentId === templateId) {
						// save item being removed for rollback
						acc.deleteHoneWalaTemplate = current;
						return acc;
					}
					// filter deleted template out of the templates lsit
					acc.bacheHua = acc.bacheHua.concat(current);
					return acc;
				},
				{
					deleteHoneWalaTemplate: {},
					bacheHua: [],
				}
			);

			this.setState({
				templates: filteredTemplates.bacheHua,
			});

			// making api request to delete template

			api.delete(templateId)
				.then(() => {
					console.log(`deleted template id is ${templateId}`);
				})
				.catch(e => {
					console.log(`there was an error removing ${templateId}`, e);
					// add temlate removed back to list
					this.setState({
						templates: filteredTemplates.bacheHua.concat(
							filteredTemplates.deleteHoneWalaTemplate
						),
					});
				});
		}
	};

	openEditor = (e, type) => {
		// 0 => create from scratch
		// 1 => edit template
		// 2 => duplicate template
		const { templates } = this.state;
		const templateId = e.target.dataset.id;

		if (templateId) {
			const filteredTemplate = templates.find(current => {
				const currentId = getTemplateId(current);
				return currentId === templateId;
			});

			const newEditorState = {
				open: true,
				type,
				template: filteredTemplate,
			};

			this.setState({
				editor: newEditorState,
			});
		} else {
			const newEditorState = {
				open: true,
				type,
				template: null,
			};

			this.setState({
				editor: newEditorState,
			});
		}
	};

	handleInputChange = event => {
		const { target } = event;
		const { value, name } = target;
		this.setState({
			[name]: value,
		});
	};
	handleCategory = event => {
		const { dataset } = event.target;
		const { name } = dataset;

		const newTypeState = Object.assign(
			{},
			{
				all: false,
				buyhatke: false,
				informvisitors: false,
				salesla: false,
				tfs: false,
				bitbns: false,
				bidforx: false,
				umc: false,
				cricketSwag: false,
				others: false,
			},
			{ [name]: true }
		);
		if (!this.state.types[name]) {
			this.setState({ types: newTypeState });
		}
	};

	render() {
		const { wordToMatch, editor, types } = this.state;
		const { open, type, template } = editor;

		return open ? (
			<Editor
				goToDashboard={this.toggleEditor}
				template={template}
				type={type}
				CopyToClipboard={this.CopyToClipboard}
				getTemplateId={getTemplateId}
			/>
		) : (
			<React.Fragment>
				<div className="nav-container">
					<div className="flex items-center">
						<img
							src={Mailer_Logo}
							alt="mailer_logo"
							css={css`
								width: 32px;
								position: relative;
								top: -2px;
							`}
						/>
						<div className="pl-2 text-2xl font-serif text-green-400">
							Banao
						</div>
					</div>
					<div
						className="flex justify-between"
						css={css`
							width: calc(100% - 230px);
						`}
					>
						<button
							type="button"
							className="c-btn c-btn--red"
							onClick={e => this.openEditor(e, 0)}
						>
							+ Create from scratch
						</button>
						<div className="relative">
							<img
								alt="search-logo"
								src={Search_Logo}
								css={css`
									width: 20px;
									top: 10px;
									left: 5px;
									z-index: 1;
									position: absolute;
								`}
							/>
							<input
								name="wordToMatch"
								type="text"
								value={wordToMatch}
								placeholder="Search"
								className="text-black relative py-2 px-8 rounded"
								onChange={this.handleInputChange}
							/>
						</div>
					</div>
				</div>
				<div className="flex">
					<div className="sidenav-wrapper">
						<div className="sidenav-container">
							{Object.keys(types).map((key, index) => (
								<div
									data-name={key}
									key={key}
									className={classNames(
										'sidenav__el',
										types[key]
											? 'sidenav__el--selected'
											: ''
									)}
									onClick={this.handleCategory}
								>
									{key}
								</div>
							))}
						</div>
					</div>
					<div className="dashboard-wrapper">
						<div className="flex flex-wrap pt-5">
							{this.renderTemplates()}
						</div>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default Dashboard;
