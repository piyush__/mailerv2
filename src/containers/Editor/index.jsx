/** @jsx jsx */
import React from "react";
import EmailEditor from "react-email-editor";
import { jsx, css } from "@emotion/core";

import api from "../../utils/api";
import html2canvas from "html2canvas";

import Mailer_Logo from "../../img/Mailer_Logo.png";
// import { importDefaultSpecifier } from '@babel/types';
// import { isBrowser } from '@emotion/utils';

export default class Editor extends React.Component {
  constructor(props) {
    super(props);
    this.editor = React.createRef();
    this.state = {
      name: "",
      type: "none",
      editing: false,
      templateId: null
    };
  }

  componentDidMount() {
    const { template, type, getTemplateId } = this.props;

    if (type && type !== 0) {
      const { name, type: prod, design } = template.data;
      const templateId = getTemplateId(template);
      if (type === 1) {
        this.setState({
          name,
          type: prod,
          editing: true,
          templateId
        });
      }
      this.loadDesign(design);
    }
  }

  loadDesign = design => {
    if (window && window.unlayer) {
      this.editor.current.loadDesign(design);
    } else {
      setTimeout(() => {
        this.loadDesign(design);
      }, 1000);
    }
  };

  handleInputChange = event => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  CopyToClipboard = () => {
    this.editor.current.exportHtml(data =>
      this.props.CopyToClipboard(data.html)
    );
  };

  exportHtml = () => {
    const { name } = this.state;
    this.editor.current.exportHtml(data => {
      const { html } = data;

      // download script
      fetch(`data:text/html;charset=utf-8,${encodeURIComponent(html)}`)
        .then(resp => resp.blob())
        .then(blob => {
          const url = window.URL.createObjectURL(blob);
          const a = document.createElement("a");
          a.style.display = "none";
          a.href = url;
          // the filename you want
          a.download = name + ".html";
          document.body.appendChild(a);
          a.click();
          window.URL.revokeObjectURL(url);
          alert(`Downloaded ${name}.html`);
        })
        .catch(() => alert("oh no!"));
    });
  };

  saveAs(uri, filename) {
    var link = document.createElement("a");

    if (typeof link.download === "string") {
      link.href = uri;
      link.download = filename;

      
      document.body.appendChild(link);

      
      link.click();

      
      document.body.removeChild(link);
    } else {
      window.open(uri);
    }
  }

  UploadNewDesign = () => {
    html2canvas(document.body).then(canvas => {
      console.log(canvas.toDataURL());
      this.saveAs(canvas.toDataURL(), "file-name.png");
    });

    const { name, type } = this.state;

    // checking if name is given or not
    if (name === "") {
      alert("Enter a name for the mailer");
      return;
    }
    // checking if type is given or not
    if (type === "none") {
      alert("Select a type for the mailer");
      return;
    }

    // getting data from editor
    this.editor.current.exportHtml(data => {
      const { html, design } = data;

      // data object to send
      const dataToSend = {
        name: name,
        type: type,
        html: html,
        design: design
      };

      // Make Api request to create new template
      api
        .create(dataToSend)
        .then(response => {
          console.log("Template successfully saved");
          console.log("response from create api", response);
          this.props.goToDashboard();
        })
        .catch(e => {
          console.log("An API error occured(create api)", e);
        });
    });
  };

  editDesign = () => {
    html2canvas(document.body).then(canvas => {
		console.log(canvas.toDataURL());
		this.saveAs(canvas.toDataURL(), "file-name.png");
	  });

    const { name, type, templateId } = this.state;

    if (!templateId) {
      return;
    }

    // checking if name is given or not
    if (name === "") {
      alert("Enter a name for the mailer");
      return;
    }
    // checking if type is given or not
    if (type === "none") {
      alert("Select a type for the mailer");
      return;
    }

    // getting data from editor
    this.editor.current.exportHtml(data => {
      const { html, design } = data;

      // data object to send
      const dataToSend = {
        name: name,
        type: type,
        html: html,
        design: design
      };

      // Make Api request to create new template
      api
        .update(templateId, dataToSend)
        .then(response => {
          console.log("Template successfully updated");
          console.log("response from update api", response);
          this.props.goToDashboard();
        })
        .catch(e => {
          console.log("An API error occured (update api)", e);
        });
    });
  };

  saveDesign = () => {
    const { editing } = this.state;
    if (editing) {
      this.editDesign();
    } else {
      this.UploadNewDesign();
    }
  };

  render() {
    const { name, type } = this.state;
    return (
      <React.Fragment>
        <div className="nav-container">
          <div className="flex items-center" onClick={this.props.goToDashboard}>
            <img
              src={Mailer_Logo}
              alt="mailer_logo"
              css={css`
                width: 32px;
                position: relative;
                top: -2px;
              `}
            />
            <div className="pl-2 text-2xl font-serif text-green-400">Banao</div>
          </div>
          <div>
            <input
              name="name"
              type="text"
              placeholder="Name Of Mailer"
              className="px-2 py-1 text-black rounded"
              value={name}
              onChange={this.handleInputChange}
            />
            <select
              name="type"
              className="h-8 w-48 text-black ml-10 bg-white"
              placeholder="Select Type"
              value={type}
              onChange={this.handleInputChange}
            >
              <option value="none">Select Type</option>
              <option value="bitbns">Bitbns</option>
              <option value="salesla">Salesla</option>
              <option value="bidforx">BidForX</option>
              <option value="buyhatke">Buyhatke</option>
              <option value="informvisitors">Inform Visitors</option>
              <option value="tfs">Tatkal For Sure</option>
              <option value="umc">UMC</option>
              <option value="cricketSwag">Cricket Swag</option>
              <option value="others">Others</option>
            </select>
          </div>
          <div className="flex">
            <button
              type="button"
              className="c-btn c-btn--red mr-2"
              onClick={this.CopyToClipboard}
            >
              Copy HTML
            </button>
            <button
              type="button"
              className="c-btn c-btn--red mr-2"
              onClick={this.exportHtml}
            >
              Download
            </button>
            <button
              type="button"
              className="c-btn c-btn--green"
              onClick={this.saveDesign}
            >
              Save Design
            </button>
          </div>
        </div>
        <div
          className="mt-16"
          css={css`
            height: calc(100vh - 4rem);
          `}
        >
          <EmailEditor
            projectId={1071}
            ref={this.editor}
            minHeight="100%"
            appearance={{
              theme: "dark",
              panels: {
                tools: {
                  dock: "right"
                }
              },
              features: {
                preview: true
              }
            }}
            options={{
              customCSS: [
                `
							// body {
							// 	background-color: yellow;
							// }
							`
              ],
              customJS: [
                window.location.protocol +
                  "//" +
                  window.location.host +
                  "/custom.js",
                `
							console.log(window.location.protocol);
							console.log(window.location.host);
							console.log('I am custom JS!');
							`
              ]
            }}
            tools={{}}
          />
        </div>
      </React.Fragment>
    );
  }
}
