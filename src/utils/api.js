/* Api methods to call /functions */

const create = (data) => {
  return fetch('/.netlify/functions/template-create', {
    body: JSON.stringify(data),
    method: 'POST'
  }).then(response => {
    return response.json()
  })
}

const readAll = () => {
  return fetch('/.netlify/functions/template-read-all').then((response) => {
    return response.json()
  })
}

const update = (templateId, data) => {
  return fetch(`/.netlify/functions/template-update/${templateId}`, {
    body: JSON.stringify(data),
    method: 'POST'
  }).then(response => {
    return response.json()
  })
}

const deleteTemplate = (templateId) => {
  return fetch(`/.netlify/functions/template-delete/${templateId}`, {
    method: 'POST',
  }).then(response => {
    return response.json()
  })
}

const batchDeleteTodo = (todoIds) => {
  return fetch(`/.netlify/functions/todos-delete-batch`, {
    body: JSON.stringify({
      ids: todoIds
    }),
    method: 'POST'
  }).then(response => {
    return response.json()
  })
}

export default {
  create: create,
  readAll: readAll,
  update: update,
  delete: deleteTemplate,
  batchDelete: batchDeleteTodo
}
