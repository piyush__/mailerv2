export default function IfElse(props) {
	const condition = props.condition || false;
	const positive = props.then || null;
	const negative = props.else || null;

	if (condition) {
		if (typeof positive === 'function') {
			return positive();
		}
		return positive;
	}
	if (typeof negative === 'function') {
		return negative();
	}
	return negative;
}
